var ip = "ws://127.0.0.1:4226/bot";
// var            ip = "ws://192.168.43.56:4226/bot";

var guid = "f5fe3664-cc88-46c5-a613-c771a34cec69";
var nomBot = "botJs";

var isNameSent = false;
var turnNb = 0;
var energyLevel = 0;
var shieldLevel = 0;
var cloakLevel = 0;
var score = 0;
var target = NaN; // Cible pour le pathfinding
var bot = NaN; // Position actuelle du bot dans la grille
var lastScan = NaN; // Numéro du round pour lequel le dernier scan date
var maxScanAge = NaN; // Nombre maximum de round pour qu'un scan soit considiré valide
var grille = NaN; // Grille pour le pathfinder
var grille2 = NaN; // Copie de la grille car le pathfinder modifie la grille
var path = NaN; // Liste des cases dans lesquelles le robot doit passer
var energies = NaN; // Liste des cellules énergie dans le scan
var ennemis = NaN; // Liste des ennemis dans le scan
var scanQueued = false; // Si un scan devra etre effectué au prochain tour
var nextScanSize = NaN; // Taille du scan 
var actions = Array(); // File des actions que devra suivre le robot
var shieldQueued = NaN; // La valeur du shield demandé
var cloakQueued = NaN; // La valeur de l'invisibilité demandée
var carte = NaN; // La carte du scan actuel
var diametre = NaN; // Rayon du diametre actuel
var nbEner = 0; // Nombre d'énergie au scan
var tir = false; // Si on cherche activement a tirer sur les ennemis
var comportement = NaN; // Le comportement du bot ()

// Collection de différentes constantes pour la file d'actions que devra suivre le bot
const haut = "Haut";
const bas = "Bas";
const gauche = "Gauche";
const droite = "Droite";
const mouvements = [haut, bas, gauche, droite];

const tirHaut = "tirHaut";
const tirBas = "tirBas";
const tirGauche = "tirGauche";
const tirDroite = "tirDroite";
const tirs = [tirHaut, tirBas, tirGauche, tirDroite];

const chgShield = "Shield";
const chgCloak = "Cloak";

const rien = "Rien"; // Ne rien faire (passer le tour en rechargeant le shield a sa valeur actuelle)

const comEnergie = "ComEnergie"; // Aller activement a la recherche d'énergie
const comAttend = "ComAttend"; // On attend de se faire tirer dessus sans bouger

// Stratégie de jeu
maxScanAge = 40; // Au bout de 40 tours, il faudra refaire un scan
scanQueued = false; // On va scanner au 2eme tour car comme nous ne pouvons pas tirer au premier round, il ne sert a rien de scanner car le bot ne bouge pas 
comportement = comEnergie;

// Au premier round, on charge le shield
shieldQueued = 30;
actions.unshift(chgShield);



var PF = require('pathfinding');
var finder = new PF.AStarFinder();

console.log("Nom du bot : " + nomBot);
console.log("GUID : " + guid);

var client = new WebSocket(ip);

client.onopen = function(event) {
    console.log('Connecté au serveur : ' + ip);
    client.send(guid);
    client.binaryType = "arraybuffer";
};

client.onmessage = function(event) {
    var response = new Uint8Array(event.data);
    switch (response[0]) {
        case 79: // O OK
            console.log("OK");
            if (!isNameSent) {
                client.send("N" + nomBot);
                console.log("Envoi du nom");
                isNameSent = true;
            }
            break;

        case 67: // C Changement dans les infos
            energyLevel = response[1] + response[2] * 256;
            shieldLevel = response[3] + response[4] * 256;
            cloakLevel = response[5] + response[6] * 256;
            score = response[7] + response[8] * 256;
            break;

        case 84: // T Nouveau tour, on doit répondre par le scan désiré
            turnNb = response[1] + response[2] * 256;
            energyLevel = response[3] + response[4] * 256;
            shieldLevel = response[5] + response[6] * 256;
            cloakLevel = response[7] + response[8] * 256;
            console.log("Début du round n°" + turnNb + "| 🏆 : " + score);
            console.log("🔋 : " + energyLevel.toString() + "| 🛡️ : " + shieldLevel.toString() + "| 💨 : " + cloakLevel.toString());
            if (scanQueued) {
                client.send(Buffer.from([68, nextScanSize])); // On envoie D et la prochaine zone a scanner
                lastScan = turnNb;
                console.log("Dernier scan : now, scan de : " + nextScanSize.toString());
            } else {
                client.send(Buffer.from([68, 0]));
                console.log("Coords : " + bot.x + ", " + bot.y);
                console.log("Dernier scan : " + (turnNb - lastScan).toString());
            }
            if (energyLevel <= 40) { // Quand on a moins de 40 d'énergie, on retire le shield + cloak
                if (cloakLevel > 0) {
                    cloakQueued = 0;
                    actions.unshift(chgCloak); // On pousse l'action en haut de la file
                }
                if (shieldLevel > 0) {
                    shieldLevel = 0;
                    actions.unshift(chgShield);
                }
            }
            if (energyLevel >= 150) { // Quand on a plus de 150 d'énergie, on retire le cloak et on met du shield
                if (cloakLevel > 0) {
                    cloakQueued = 0;
                    actions.unshift(chgCloak);
                }
                if (shieldLevel < 20) {
                    shieldQueued = 20;
                    actions.unshift(chgShield);
                }
            }
            if (energyLevel >= 200) { // Quand on a plus de 200 d'énergie, on met 50 de shield
                if (shieldLevel < 30) { // On charge le shield que si il est en dessous de 30, ce qui permet au bot de se déplacer au lieu de devoir recharger constamment son shield
                    shieldQueued = 50;

                }
            }
            break;

        case 73: // I On reçoit le scan de la map
            if (scanQueued) {
                scanQueued = false;
                diametre = response[1];
                carte = new Array(diametre);
                energies = new Array();
                grille = new PF.Grid(diametre, diametre);
                nbEner = 0;
                bot = {
                    x: (diametre - 1) / 2,
                    y: (diametre - 1) / 2
                }; // Le bot est forcément au milieu du scan
                ennemis = new Array();
                for (x = 0; x < diametre; x++) {
                    carte[x] = new Array(diametre);
                    for (y = 0; y < diametre; y++) {
                        var caseActuelle = response[2 + (x * diametre) + y];
                        carte[x][y] = caseActuelle;
                        if (caseActuelle === 3) { // Si la case est une énergie, on la met dans une liste
                            energies.push({
                                x: x,
                                y: y
                            });
                            nbEner++;
                        } else if (caseActuelle === 4) { // Si la case est un robot
                            if ((bot.x === x) && (bot.y === y)) { // Si la case est notre robot
                                carte[x][y] = 0; // On ne met pas le robot sur la carte afin de pouvoir réutiliser la carte sans avoir a déplacer un 4 dans le tableau
                            } else { // Si ce n'est pas notre robot, alors c'est un ennemi
                                ennemis.push({
                                    x: x,
                                    y: y
                                });
                            }
                        } else if (caseActuelle === 2) { // Si c'est un mur, alors il faut inquer au pathfinder que nous ne pouvons pas marcher dessus
                            grille.setWalkableAt(x, y, false);
                        }
                    }
                }
                if (turnNb != 1) {
                    var result = checkTir(ennemis, bot);
                    if (result) {
                        actions.unshift(result);
                    }
                }
            }
            if (!actions.length) { // Quand le robot n'a plus rien a faire
                if (energies.length != 0) {
                    console.log("PATHFIND---");
                    console.log(bot);
                    console.log(energies);
                    target = nearest(energies, bot);
                    console.log(target);
                    grille2 = grille.clone();
                    console.log("De : " + bot.x + ", " + bot.y + "   Vers : " + target.x + ", " + target.y);
                    path = finder.findPath(bot.x, bot.y, target.x, target.y, grille2);
                    // Le pathfinder nous retourne les coordonnées des points que le bot devra suivre pour aller a l'énergie
                    // Nous devons alors les convertirs en mouvements haut, bas, gauche, droite
                    // Le premier element de path est la position actuelle du robot, mais celle-ci est déjà dans la variable bot
                    path.shift();
                    var currentPos = [bot.x, bot.y];
                    path.forEach(function(pos, index) {
                        var x = pos[0] - currentPos[0]; // Le mouvement vertical
                        var y = pos[1] - currentPos[1]; // Le mouvement horizontal
                        if (x === -1) {
                            actions.push(haut);
                        } else if (y === -1) {
                            actions.push(gauche);
                        } else if (x === 1) {
                            actions.push(bas);
                        } else if (y === 1) {
                            actions.push(droite);
                        } else {
                            console.log("Erreur du traitement du chemin");
                        }
                        currentPos = pos;
                    });
                } else {
                    for (var i = 0; i < 5; i++) {
                        actions.push(rien);
                    }
                    console.log("Il n'y a pas d'énergies dans le scan, on fait rien pendant 5 tours");
                }
            }

            if (actions.length > 0) { // Cette partie s'occupe de 
                var action = actions.shift();
                switch (action) {
                    case haut:
                        client.send(Buffer.from([1, 1]));
                        bot.x--;
                        break;
                    case bas:
                        client.send(Buffer.from([1, 3]));
                        bot.x++;
                        break;
                    case gauche:
                        client.send(Buffer.from([1, 4]));
                        bot.y--;
                        break;
                    case droite:
                        client.send(Buffer.from([1, 2]));
                        bot.y++;
                        break;
                    case tirHaut:
                        client.send(Buffer.from([4, 1]));
                        break;
                    case tirBas:
                        client.send(Buffer.from([4, 3]));
                        break;
                    case tirGauche:
                        client.send(Buffer.from([4, 4]));
                        break;
                    case tirDroite:
                        client.send(Buffer.from([4, 2]));
                        break;
                    case chgShield:
                        if (isNaN(shieldQueued)) { // Si on a demandé un shield mais pas de valeur a été donnée
                            console.log("UN SHIELD A ÉTÉ DEMANDÉ MAIS PAS DE VALEUR A ÉTÉ DONNÉE");
                            console.log("DEFAUT : 15");
                            shieldQueued = 15;
                        }
                        client.send(Buffer.from([2, shieldQueued, 0])); // Il faut envoyer le shield sur 2 octets
                        shieldQueued = NaN;
                        break;
                    case chgCloak:
                        if (isNaN(cloakQueued)) { // Si on a demandé de l'invisibilité mais pas de valeur a été donnée
                            console.log("L'INVISIBILITÉ A ÉTÉ DEMANDÉE MAIS PAS DE VALEUR A ÉTÉ DONNÉE");
                            console.log("DEFAUT : 15");
                            cloakQueued = 15;
                        }
                        client.send(Buffer.from([3, cloakQueued, 0]));
                        cloakQueued = NaN;
                        break;
                    case rien:
                        // On ne fait rien
                        client.send(Buffer.from([2, shieldLevel, 0]));
                    default:
                        console.log("ACTION NON RECONNUE");
                }
                if (mouvements.includes(action)) { // Si on vient de se déplacer
                    if (contain(energies, bot)) { // Si on est sur une case d'énergie (on ne peut pas utiliser '.includes()' car bot est un objet et ce n'est pas compatible)
                        console.log("ENERGIE RECUPÉRÉE");
                        carte[bot.x][bot.y] = 0; // On la retire de la carte
                        // Et on la retire de la liste des energies
                        var index = whereIs(energies, bot);
                        if (index > -1) {
                            energies.splice(index, 1);
                        } else {
                            console.log("ERREUR LORS DU SPLICE");
                        }
                    }
                }
            }
            if (((turnNb != 0) && isNaN(lastScan)) || ((turnNb - lastScan) >= maxScanAge) || (((nbEner / 2) > energies.length)) && comportement == comEnergie) {
                // Si on est au deuxième tour ou si l'age maximal du scan a été atteint ou si on a consommé plus de la motié des énergies dispo en mode energie
                if (comportement = comEnergie) {
                    nextScanSize = 6;
                    scanQueued = true;
                } else if (comportement = comAttend) {
                    nextScanSize = 0;
                    scanQueued = true;
                }
            }

            break;

        case 68: // D Mort
            // On est mort ;(
            console.log("😭");
            client.close();
            break;

        default: // En cas de message inconnu
            console.log("Trame non reconnue : ");
            console.log(event);
            console.log(response[0]);
    }
}

function nearest(objects, origin) { // Cette fontion renvoie l'objet le plus proche de l'origine
    var nearest = NaN;
    var distNearest = 9999;
    objects.forEach(function(obj, index) {
        distance = Math.abs(obj.x - origin.x) + Math.abs(obj.y - origin.y);
        if (distNearest > distance) { // Si l'objet est plus proche que l'ancien
            nearest = obj;
            distNearest = distance;
        }
    });
    return nearest;
}

function whereIs(tabl, elem) { // Renvoie l'index d'un element dans un tableau d'objets 
    for (i = 0; i < tabl.length; i++) {
        if (tabl[i].x === elem.x) {
            if (tabl[i].y === elem.y) {
                return i;
            }
        }
    }
    return -1;
}

function contain(tabl, elem) { // Renvoir true si elem est dans tabl
    var i;
    for (i = 0; i < tabl.length; i++) {
        if (tabl[i].x === elem.x) {
            if (tabl[i].y === elem.y) {
                return true;
            }
        }
    }

    return false;
}

function makeid(length) { // La fonction génére un nom aléatoire pour le bot (utile pour les tests)
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function canShoot(ennemi, bot) { // La fonction renvoie tir{haut, bas, gauche, droite} quand le bot est aligné avec l'ennemi, sinon false
    var x = ennemi.x - bot.x;
    var y = ennemi.y - bot.y;
    if (x != 0 && y != 0) { // Si le l'ennemi est en diagonale, on ne peut pas tirer
        return false;
    }
    if (x <= -1) {
        return tirHaut;
    } else if (y <= -1) {
        return tirGauche;
    } else if (x >= 1) {
        return tirBas;
    } else if (y >= 1) {
        return tirDroite;
    } else {
        console.log("Erreur du tir");
    }
}

function checkTir(ennemis, bot) { // La fonction boucle tous les ennemis et renvoie une action a faire pour tirer sur un ennemi (si possible), sinon false
    var action = false;
    ennemis.forEach(function(ennemi, index) {
        action = canShoot(ennemi, bot);
    });
    return action;
}